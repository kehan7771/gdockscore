# GDockScore

GDockScore is a protein-protein docking scoring function that utilizes graph representations of protein complexes. It achieves state-of-the-art performance on the CAPRI score set a gold-standard data set for developing protein-protein docking scoring functions.

## Installation

Installation should take less than 10 minutes. Its recommended that install `gdockscore` in a clean python virtual environment (Python >= 3.8, tested with Python 3.9.7). GDockScore was tested on Ubuntu (18.04).
 
1. Clone the repository `git clone https://gitlab.com/mcfeemat/gdockscore.git`
2. Change directories to the gdockscore repository `cd gdockscore`
4. Install requirements `pip install -r requirements.txt`
5. Install package `pip install .`

## Scoring decoys with GDockScore

To score an interface change to the gdockscore install folder and run the following command:
`python ./scripts/run_gdockscore.py --file your_pdb_file.pdb --chain_pair AB

The script takes as input the location of a PDB file containing two interfaced proteins (`--file`), and a chain pair in the form Chain1Chain2 (`--chain_pair`).The model output will be written to a file called results.txt. The model should not take more than a few minutes to complete running on a GPU. Notes: your file may fail to ouput a score if your proteins are too fart apart according to our model standards. The chain pair must be in the order in which the chains appear in the PDB file. For example, if chain A appears before B in the file then --chain_pair should be AB.

An example PDB and expected output are provided in the example folder.

Paper: https://doi.org/10.1101/2022.12.02.518908 
