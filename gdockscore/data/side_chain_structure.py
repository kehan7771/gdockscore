"""
Methods for determining the chi angles of an amino acid inputs side chain. The goal of
using side chain chi angles and coordinates is to implement the a full atom, all angle 
representation of side chains for input into this machine learning model.

TODO: This code uses lists and converts them to numpy arrays which is  slightly
less efficient than creating an array with np.zeros and then adjusting the
values of each row iteratively. This should be done in the future. Cleaner filtering
of hydrogen atoms.
"""

from Bio import PDB
import numpy as np
import argparse
import pickle

# import logging
# logging.basicConfig(filename="side_chain_error.log", level=logging.ERROR)
np.warnings.filterwarnings(
    "ignore", category=np.VisibleDeprecationWarning
)  # Suppress ragged warnings

# Set necessary global variables
pdb_parser = PDB.PDBParser(QUIET=True)
possible_chi = ["chi1", "chi2", "chi3", "chi4", "chi5"]
# These atoms are for part of the backbone and should be ignored
ignored_atoms = ["N", "CA", "C", "O", "OXT"]  # Also include OXT
digits = "1234567890"  # Digits to ignore H atoms

# Largest sidechain will be TRP which will have 10 atoms listed in the PDB file
# with 3 coordinates each
MAX_ATOMS = 10


def convert_angles_to_radians(chi_list):
    """
    Converts degress to radians
    """
    radians_list = [item * (np.pi / 180) for item in chi_list]

    return radians_list


def pad_chi_angles(chi_list):
    """
    Each residue may have a different number of chi angles and if the chi
    angle does not exist biopython produces a None for that entry. This code
    will change the Nones to 0s
    """
    for i, chi in enumerate(chi_list):
        if chi is None:
            chi_list[i] = 0

    return chi_list


def fetch_chi_angles(pdb_path, units="degrees"):
    """
    Find the chi angles of the sides chains. Each amino acid side chain will
    have a maximum of 5 chi angles.
    """
    structure = pdb_parser.get_structure("input", pdb_path)
    structure = structure[0]
    all_chain_chis = {}

    for chain in structure:
        ic = PDB.internal_coords.IC_Chain(chain)
        # Calculate internal coordinates for chain
        ic.atom_to_internal_coordinates()
        # if len(ic.ordered_aa_ic_list) > 0:
        # logging.error(
        #     "The chain is valid... calculating chi angles for all residues"
        # )
        # else:
        if len(ic.ordered_aa_ic_list) == 0:  # Make sure chain is not empty
            # logging.error(
            #     "The selected chain appears to be empty... please check the input PDB file... processing the next chain"
            # )
            all_chain_chis[chain.id] = None
            continue

        residues = ic.ordered_aa_ic_list
        chis = []

        for residue in residues:
            if not residue.is20AA:
                continue
            current_chis = []
            for chi in possible_chi:
                try:
                    angle = residue.get_angle(chi)
                    current_chis.append(angle)
                # Probably can remove this
                except KeyError:
                    # Handle glycines
                    if chi == "chi1":
                        angle = 0
                        # current_chis = np.append(current_chis, angle)
                        current_chis.append(angle)
                    break

            if units == "radians":
                current_chis = convert_angles_to_radians(current_chis)

            current_chis = pad_chi_angles(current_chis)
            chis.append(current_chis)

        if len(chis) == len(ic.ordered_aa_ic_list):
            chis = np.vstack(chis)
            all_chain_chis[chain.id] = chis
        else:
            # logging.error(
            #     "Chi angles for residues in the chain are missing... moving to next chain"
            # )
            all_chain_chis[chain.id] = None
            continue

    return all_chain_chis


def pad_coordinates(array, type):
    """
    Pad the atom coordinates to be the same length as different side chains
    will have a different total number of atoms
    """
    # Every atom has 3 coordinates
    diff = abs(len(array) - MAX_ATOMS)

    if type == "coords":
        pad = [0, 0, 0]
        for i in range(diff):
            array.append(pad)

    elif type == "distances":  # Could change to else
        pad = diff * [0]
        array.extend(pad)

    return array


def fetch_atom_distances(pdb_path):
    """
    Finds the coordinates of all the atoms in the side chain of the amino acid
    residue and computes the distance to the alpha carbon of the residues. These
    coordinates will need to converted to the frame of reference of its
    neighbors in the main compute features program
    """
    # Get structure
    structure = pdb_parser.get_structure("input", pdb_path)
    structure = structure[0]

    chain_dict_coords = {}
    chain_dict_distances = {}

    for chain in structure:
        chain_atoms_coords = []
        chain_distances = []

        for residue in chain:
            if not PDB.Polypeptide.is_aa(residue.get_resname(), standard=True):
                continue

            atoms = residue.get_atoms()

            try:
                alpha_carbon = residue["CA"].get_coord()
            except KeyError:
                continue

            current_coords = []
            current_distances = []

            # Could replace with regex
            for atom in atoms:
                if atom.get_name() not in ignored_atoms:
                    # Rosetta appears to add hydrogens which should be ignored
                    if (
                        atom.get_name().startswith("H") is False
                        and atom.get_name()[0] not in digits
                    ):
                        coord = atom.coord
                        current_coords.append(coord)

                        distance = np.linalg.norm(coord - alpha_carbon)
                        distance = distance.tolist()
                        current_distances.append(distance)

            current_coords = pad_coordinates(current_coords, "coords")
            current_distances = pad_coordinates(current_distances, "distances")

            chain_atoms_coords.append(current_coords)
            chain_distances.append(current_distances)

        chain_atoms_coords = np.array(chain_atoms_coords)
        chain_distances = np.array(chain_distances)

        # Make sure to raise an assertion error if one or more of the side chains has
        # invalid atoms that have not been accounted for
        assert chain_atoms_coords.dtype is not np.dtype(
            "object"
        ) and chain_distances.dtype is not np.dtype(
            "object"
        ), "One or more side chains has invalid atoms"

        chain_dict_coords[chain.id] = chain_atoms_coords
        chain_dict_distances[chain.id] = chain_distances

    return chain_dict_coords, chain_dict_distances


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--pdb_file",
        dest="pdb_file",
        type=str,
        required=True,
        help="The path to the pdb file for chi angle calculations",
    )
    parser.add_argument(
        "--chi_out_file",
        dest="chi_out_file",
        type=str,
        required=False,
        help="Store the chi angle dictionary in a pickle if needed",
    )
    parser.add_argument(
        "--atom_out_file",
        dest="atom_out_file",
        type=str,
        required=False,
        help="Store the sidechain atom coordinates in a pickle if needed",
    )

    args = parser.parse_args()
    pdb_file = args.pdb_file
    chi_out_file = args.chi_out_file
    atom_out_file = args.atom_out_file

    # Store chi angles in file
    chis = fetch_chi_angles(pdb_file)
    coords, distances = fetch_atom_distances(pdb_file)

    print(chis["B"].shape)
    print(distances["B"].shape)
    print(coords["B"].shape)

    # Save files
    if chi_out_file is not None:
        with open(chi_out_file, "wb") as file:
            pickle.dump(chis, file)

    if atom_out_file is not None:
        with open(atom_out_file, "wb") as file:
            pickle.dump(distances, file)
