"""Build a CSV containing the file locations for the model dataset."""

import os
import argparse
import pandas as pd


def find_paths(root_dir, file_name):
    """Find the file paths of files in root directory"""
    entries = []
    for dir_path, subdirs, files in os.walk(root_dir):
        for file in files:
            path = os.path.join(dir_path, file)
            name = file[:-4]

            entry = name + "," + path + "\n"

            entries.append(entry)

    with open(file_name, "w") as out_file:
        header = "File, Path" + "\n"
        for value in entries:
            out_file.write(value + "\n")

    return None


if __name__ == "__main__":
    # Set parser arguments
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--root_dir",
        dest="root_dir",
        required=True,
        type=str,
        help="The location of the files to append",
    )

    parser.add_argument(
        "--file_name",
        dest="file_name",
        required=True,
        type=str,
        help="The file name of the created file containing the file paths",
    )

    args = parser.parse_args()
    root_dir = args.root_dir
    file_name = args.file_name

    find_paths(root_dir, file_name)

    df = pd.read_csv(file_name, header=0)

    df.to_csv("paths.csv", index=False)
