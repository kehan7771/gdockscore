"""
The complete model implemented in PyTorch. This was originally
written by Abdin et al. for the PepNN-struct architecture and has been adjusted
to utilize bidirectional multi-head graph attention.
"""

import torch
import torch.nn as nn
from gdockscore.models.layers import *
from gdockscore.models.modules import *


def to_var(x):
    if torch.cuda.is_available():
        x = x.cuda()
    return x


def get_neighbor_features(nodes, neighbor_idx):
    """
    Function from Ingraham et al. 2019 source code
    """
    # Features [B,N,C] at Neighbor indices [B,N,K] => [B,N,K,C]
    # Flatten and expand indices per batch [B,N,K] => [B,NK] => [B,NK,C]
    neighbors_flat = neighbor_idx.view((neighbor_idx.shape[0], -1))
    neighbors_flat = neighbors_flat.unsqueeze(-1).expand(-1, -1, nodes.size(2))
    # Gather and re-pack
    neighbor_features = torch.gather(nodes, 1, neighbors_flat)
    neighbor_features = neighbor_features.view(list(neighbor_idx.shape)[:3] + [-1])

    return neighbor_features


def cat_neighbors_nodes(nodes, neighbors, neighbor_indices):
    """Cat the neighbor edges to appropriate nodes"""
    nodes = get_neighbor_features(nodes, neighbor_indices)
    edge_features = torch.cat([neighbors, nodes], -1)

    return edge_features


class RepeatedModule(nn.Module):
    def __init__(
        self,
        edge_features,
        node_features,
        int_features,
        n_layers,
        d_model,
        n_head,
        d_k,
        d_v,
        d_inner,
        dropout=0.1,
    ):
        super().__init__()

        self.edge_embedding = nn.Linear(edge_features, d_model)
        self.node_embedding = nn.Linear(node_features, d_model)
        self.interface_edge_embedding = nn.Linear(int_features, d_model)

        # self.edge_embedding_2 = nn.Linear(edge_features, d_model)
        # self.node_embedding_2 = nn.Linear(node_features, d_model)
        # self.interface_edge_embedding_2 = nn.Linear(int_features, d_model)

        self.d_model = d_model

        self.exchange_layer_stack = nn.ModuleList(
            [
                ExchangeLayer(d_model, d_inner, n_head, d_k, d_v, dropout=dropout)
                for _ in range(n_layers)
            ]
        )

        self.dropout_1 = nn.Dropout(dropout)
        self.dropout_2 = nn.Dropout(dropout)
        self.dropout_3 = nn.Dropout(dropout)

    def forward(
        self,
        nodes,
        edges,
        neighbor_indices,
        nodes_2,
        edges_2,
        neighbor_indices_2,
        interface_idx,
        int_edges,
        int_neigh_idx,
        interface_idx_2,
        int_edges_2,
        int_neigh_idx_2,
    ):
        graph_attention_list = []
        graph_2_attention_list = []

        int_attention_list = []
        int_2_attention_list = []

        encoded_edges = self.edge_embedding(edges)
        encoded_nodes = self.node_embedding(nodes)
        encoded_int = self.interface_edge_embedding(int_edges)

        encoded_edges_2 = self.edge_embedding(edges_2)
        encoded_nodes_2 = self.node_embedding(nodes_2)
        encoded_int_2 = self.interface_edge_embedding(int_edges_2)

        node_enc = encoded_nodes
        node_enc_2 = encoded_nodes_2

        edge_input = cat_neighbors_nodes(encoded_nodes, encoded_edges, neighbor_indices)
        edge_input_2 = cat_neighbors_nodes(
            encoded_nodes_2, encoded_edges_2, neighbor_indices_2
        )

        int_edges_input = cat_neighbors_nodes(node_enc_2, encoded_int, int_neigh_idx)
        int_edges_input_2 = cat_neighbors_nodes(
            node_enc, encoded_int_2, int_neigh_idx_2
        )

        node_enc = self.dropout_1(node_enc)
        edge_input = self.dropout_2(edge_input)

        node_enc_2 = self.dropout_1(node_enc_2)
        edge_input_2 = self.dropout_2(edge_input_2)

        int_edges_input = self.dropout_3(int_edges_input)
        int_edges_input_2 = self.dropout_3(int_edges_input_2)

        for exchange_layer in self.exchange_layer_stack:
            (
                node_enc,
                node_enc_2,
                graph_attention,
                graph_2_attention,
                int_attention,
                int_2_attention,
            ) = exchange_layer(
                node_enc,
                edge_input,
                node_enc_2,
                edge_input_2,
                interface_idx,
                interface_idx_2,
                int_edges_input,
                int_edges_input_2,
            )

            graph_attention_list.append(graph_attention)

            graph_2_attention_list.append(graph_2_attention)

            int_attention_list.append(int_attention)

            int_2_attention_list.append(int_2_attention)

            edge_input = cat_neighbors_nodes(node_enc, encoded_edges, neighbor_indices)
            edge_input_2 = cat_neighbors_nodes(
                node_enc_2, encoded_edges_2, neighbor_indices_2
            )

            int_edges_input = cat_neighbors_nodes(
                node_enc_2, encoded_int, int_neigh_idx
            )
            int_edges_input_2 = cat_neighbors_nodes(
                node_enc, encoded_int_2, int_neigh_idx_2
            )

        return (
            node_enc,
            node_enc_2,
            graph_attention_list,
            graph_2_attention_list,
            int_attention_list,
            int_2_attention_list,
        )


class FullModel(nn.Module):
    def __init__(
        self,
        edge_features,
        node_features,
        int_features,
        n_layers,
        d_model,
        n_head,
        d_k,
        d_v,
        d_inner,
        dropout=0.1,
        return_attention=False,
    ):
        super().__init__()

        self.repeated_module = RepeatedModule(
            edge_features,
            node_features,
            int_features,
            n_layers,
            d_model,
            n_head,
            d_k,
            d_v,
            d_inner,
            dropout=dropout,
        )
        self.final_ffn = FFN(d_model, d_inner, dropout=dropout)
        self.final_pool = nn.AdaptiveMaxPool1d(1)
        self.final_linear = nn.Linear(d_inner, 1)

        self.return_attention = return_attention

    def forward(
        self,
        nodes,
        edges,
        neighbor_indices,
        nodes_2,
        edges_2,
        neighbor_indices_2,
        interface_idx,
        int_edges,
        int_neigh_idx,
        interface_idx_2,
        int_edges_2,
        int_neigh_idx_2,
    ):
        (
            node_enc,
            node_enc_2,
            graph_attention_list,
            graph_2_attention_list,
            int_attention_list,
            int_2_attention_list,
        ) = self.repeated_module(
            nodes,
            edges,
            neighbor_indices,
            nodes_2,
            edges_2,
            neighbor_indices_2,
            interface_idx,
            int_edges,
            int_neigh_idx,
            interface_idx_2,
            int_edges_2,
            int_neigh_idx_2,
        )
        node_enc = torch.cat((node_enc, node_enc_2), 1)

        node_enc = self.final_ffn(node_enc)
        node_enc = node_enc.transpose(1, 2)

        node_enc = self.final_pool(node_enc)
        node_enc = self.final_linear(node_enc.transpose(1, 2))

        output = node_enc

        if not self.return_attention:
            return output
        else:
            return (
                output,
                graph_attention_list,
                graph_2_attention_list,
                int_attention_list,
                int_2_attention_list,
            )
