"""
This is a modification of the original reciprocal attention layer
implemented by Abdin et al. Here we have a single multi-head graph attention
layer that takes node and edge embeddings from two proteins and produces
interface residue updates for the other protein residue embedding.

Notes: Output of direction 1 of bidirectional layer updates protein 1 now and output of
direction 2 updates interface nodes in protein 2 embedding. It has to be this way
because the output of direction 1 is # interface res in prot 1 x features and the output
of direction 2 is # interface res in prot 2 x features. If the number of interface
residues is not the same it does not make sense to add output of direction 1 to the
interface nodes of 2 and vice versa.

TODO: Updates to interface nodes needs to be updated to work with batches properly
"""

from torch import index_select
from torch import nn
from gdockscore.models.modules import (
    MultiHeadAttentionExchange,
    MultiHeadAttentionGraph,
    FFN,
)


class ExchangeLayer(nn.Module):
    """Exchange layer that provides interface nodes updates"""

    def __init__(self, d_model, d_inner, n_head, d_k, d_v, dropout=0.1):
        super().__init__()

        self.graph_attention_layer = MultiHeadAttentionGraph(
            n_head, d_model, d_k, d_v, dropout=dropout
        )

        self.exchange_attention_layer = MultiHeadAttentionExchange(
            n_head, d_model, d_k, d_v, dropout=dropout
        )

        self.ffn_graph = FFN(d_model, d_inner)

    def forward(
        self,
        nodes,
        edges,
        nodes_2,
        edges_2,
        interface_idx,
        interface_idx_2,
        int_edges,
        int_edges_2,
    ):
        node_enc, graph_attention = self.graph_attention_layer(nodes, edges)
        node_enc_2, graph_attention_2 = self.graph_attention_layer(nodes_2, edges_2)

        node_enc_int = index_select(node_enc, 1, interface_idx.squeeze())
        node_enc_int_2 = index_select(node_enc_2, 1, interface_idx_2.squeeze())

        (
            node_enc_up,
            node_enc_up_2,
            ex_attention_1,
            ex_attention_2,
        ) = self.exchange_attention_layer(
            node_enc_int, int_edges, node_enc_int_2, int_edges_2
        )

        # Currently hotfixed to work with batch size 1 only
        for i, idx in enumerate(interface_idx[0]):
            node_enc[:, idx, :] = node_enc_up[:, i, :]

        for i, idx in enumerate(interface_idx_2[0]):
            node_enc_2[:, idx, :] = node_enc_up_2[:, i, :]

        node_enc = self.ffn_graph(node_enc)
        node_enc_2 = self.ffn_graph(node_enc_2)

        return (
            node_enc,
            node_enc_2,
            graph_attention,
            graph_attention_2,
            ex_attention_1,
            ex_attention_2,
        )
