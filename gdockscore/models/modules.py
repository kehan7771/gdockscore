"""
A modification of Abdin et al.'s PepNN-struct architecture. The
model has been adjusted to use two protein inputs with Ingraham et al.'s protein
graph encoder
"""

from torch import nn
import numpy as np
import torch
import torch.nn.functional as F


def to_var(x):
    """Push data to GPU memory"""
    if torch.cuda.is_available():
        x = x.cuda()
    return x


class MultiHeadAttentionGraph(nn.Module):
    """
    Class for the multi-head attention layer used in the encoder, this has to
    take into account the fact that attention is only taken over edges that
    are incident to the node in the structure encoding
    """

    def __init__(self, n_head, d_model, d_k, d_v, dropout=0.1):
        super().__init__()

        self.n_head = n_head
        self.d_model = d_model
        self.d_k = d_k
        self.d_v = d_v

        self.W_Q = nn.Linear(d_model, n_head * d_k)

        # account for the fact that the relational edge information has double
        # the length
        self.W_K = nn.Linear(d_model * 2, n_head * d_k)
        self.W_V = nn.Linear(d_model * 2, n_head * d_v)
        self.W_O = nn.Linear(n_head * d_v, d_model)

        self.softmax = nn.Softmax(dim=-1)

        self.layer_norm = nn.LayerNorm(d_model)

        self.dropout = nn.Dropout(dropout)

    def forward(self, nodes, edges):

        n_batch, n_nodes, n_neighbors = edges.shape[:3]

        Q = self.W_Q(nodes).view([n_batch, n_nodes, 1, self.n_head, 1, self.d_k])
        K = self.W_K(edges).view(
            [n_batch, n_nodes, n_neighbors, self.n_head, self.d_k, 1]
        )

        attention = (
            torch.matmul(Q, K)
            .view([n_batch, n_nodes, n_neighbors, self.n_head])
            .transpose(-2, -1)
        )

        attention = attention / np.sqrt(self.d_k)

        attention = self.softmax(attention)

        V = (
            self.W_V(edges)
            .view([n_batch, n_nodes, n_neighbors, self.n_head, self.d_v])
            .transpose(2, 3)
        )

        attention = attention.unsqueeze(-2)

        output = torch.matmul(attention, V).view(
            [n_batch, n_nodes, self.d_v * self.n_head]
        )

        output = self.W_O(output)
        output = self.dropout(output)
        output = self.layer_norm(output + nodes)

        attention = attention.squeeze(-2).transpose(-2, -1)

        return output, attention


class MultiHeadAttentionSequence(nn.Module):
    """
    Class implementing standard multi-head attention. This module is used during
    the final part of the model after the bidirectional layers have been used
    """

    def __init__(self, n_head, d_model, d_k, d_v, dropout=0.1):
        super().__init__()

        self.n_head = n_head
        self.d_model = d_model
        self.d_k = d_k
        self.d_v = d_v

        self.W_Q = nn.Linear(d_model, n_head * d_k)
        self.W_K = nn.Linear(d_model, n_head * d_k)
        self.W_V = nn.Linear(d_model, n_head * d_v)
        self.W_O = nn.Linear(n_head * d_v, d_model)

        self.layer_norm = nn.LayerNorm(d_model)

        self.dropout = nn.Dropout(dropout)

    def forward(self, q, k, v):
        batch, len_q, _ = q.size()
        batch, len_k, _ = k.size()
        batch, len_v, _ = v.size()

        Q = self.W_Q(q).view([batch, len_q, self.n_head, self.d_k])
        K = self.W_K(k).view([batch, len_k, self.n_head, self.d_k])
        V = self.W_V(v).view([batch, len_v, self.n_head, self.d_v])

        Q = Q.transpose(1, 2)
        K = K.transpose(1, 2).transpose(2, 3)
        V = V.transpose(1, 2)

        attention = torch.matmul(Q, K)
        attention = attention / np.sqrt(self.d_k)
        attention = F.softmax(attention, dim=-1)

        output = torch.matmul(attention, V)
        output = output.transpose(1, 2).reshape([batch, len_q, self.d_v * self.n_head])
        output = self.W_O(output)
        output = self.dropout(output)
        output = self.layer_norm(output + q)

        return output, attention


class MultiHeadAttentionExchange(nn.Module):
    """
    Class implementing multi-head exchange attention. This is a
    modification of the concept of reciprocal attention implemented by Abdin
    et al. Here a single multi-head block attention recieves two sets of
    inputs. The interface nodes of A and (interface nodes in A, interface edges
    A -> B and for the other direction interface nodes of B and (interface nodes
    in A, interface edges B -> A
    """

    def __init__(self, n_head, d_model, d_k, d_v, dropout=0.1):
        super().__init__()

        self.n_head = n_head
        self.d_model = d_model
        self.d_k = d_k
        self.d_v = d_v

        self.W_Q = nn.Linear(d_model, n_head * d_k)
        self.W_K = nn.Linear(d_model * 2, n_head * d_k)
        self.W_V = nn.Linear(d_model * 2, n_head * d_v)
        self.W_O = nn.Linear(n_head * d_v, d_model)

        self.softmax = nn.Softmax(dim=-1)

        self.layer_norm = nn.LayerNorm(d_model)
        self.dropout = nn.Dropout(dropout)

        # self.layer_norm_2 = nn.LayerNorm(d_model)
        # self.dropout_2 = nn.Dropout(dropout)

    def forward(self, nodes, edges, nodes_2, edges_2):
        n_batch, n_nodes, n_neighbors = edges.shape[:3]
        n_batch_2, n_nodes_2, n_neighbors_2 = edges_2.shape[:3]

        # Direction 1
        Q = self.W_Q(nodes).view([n_batch, n_nodes, 1, self.n_head, 1, self.d_k])
        K = self.W_K(edges).view(
            [n_batch, n_nodes, n_neighbors, self.n_head, self.d_k, 1]
        )

        attention = (
            torch.matmul(Q, K)
            .view([n_batch, n_nodes, n_neighbors, self.n_head])
            .transpose(-2, -1)
        )

        attention = attention / np.sqrt(self.d_k)
        attention = self.softmax(attention)

        V = (
            self.W_V(edges)
            .view([n_batch, n_nodes, n_neighbors, self.n_head, self.d_v])
            .transpose(2, 3)
        )

        attention = attention.unsqueeze(-2)

        output = torch.matmul(attention, V).view(
            [n_batch, n_nodes, self.d_v * self.n_head]
        )

        attention = attention.squeeze(-2).transpose(-2, -1)

        # Direction 2
        Q_2 = self.W_Q(nodes_2).view(
            [n_batch_2, n_nodes_2, 1, self.n_head, 1, self.d_k]
        )
        K_2 = self.W_K(edges_2).view(
            [n_batch_2, n_nodes_2, n_neighbors_2, self.n_head, self.d_k, 1]
        )

        attention_2 = (
            torch.matmul(Q_2, K_2)
            .view([n_batch_2, n_nodes_2, n_neighbors_2, self.n_head])
            .transpose(-2, -1)
        )

        attention_2 = attention_2 / np.sqrt(self.d_k)
        attention_2 = self.softmax(attention_2)

        V_2 = (
            self.W_V(edges_2)
            .view([n_batch_2, n_nodes_2, n_neighbors_2, self.n_head, self.d_v])
            .transpose(2, 3)
        )

        attention_2 = attention_2.unsqueeze(-2)

        output_2 = torch.matmul(attention_2, V_2).view(
            [n_batch_2, n_nodes_2, self.d_v * self.n_head]
        )

        attention_2 = attention_2.squeeze(-2).transpose(-2, -1)

        # Project to output
        output = self.W_O(output)
        output_2 = self.W_O(output_2)

        output = self.dropout(output)
        output = self.layer_norm(output + nodes)

        output_2 = self.dropout(
            output_2
        )  # Reuse the same dropout since both outputs use the same Wo matrix
        output_2 = self.layer_norm(output_2 + nodes_2)

        return output, output_2, attention, attention_2


class FFN(nn.Module):
    """Position-wise feed forward layer implemented with 1D convolutions"""

    def __init__(self, d_in, d_hid, dropout=0.1):
        super().__init__()

        self.layer_1 = nn.Conv1d(d_in, d_hid, 1)
        self.layer_2 = nn.Conv1d(d_hid, d_in, 1)
        self.relu = nn.ReLU()
        self.layer_norm = nn.LayerNorm(d_in)

        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        residual = x

        output = self.layer_1(x.transpose(1, 2))
        output = self.relu(output)
        output = self.layer_2(output)
        output = self.dropout(output)
        output = self.layer_norm(output.transpose(1, 2) + residual)

        return output
